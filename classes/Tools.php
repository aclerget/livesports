<?php
/**
 * User: axel
 * Date: 06/02/15
 * Time: 13:35
 */

namespace classes;

class Tools {

    public static function getDataToJsonRequest()
    {
        return json_decode(file_get_contents('php://input'));
    }
}