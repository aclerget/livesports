<?php
/**
 * User: Maxwell
 * Date: 10/01/2015
 * Time: 14:28
 */

namespace classes;

use Exception;

/**
 * Class Post
 */
class Post
{


    /**
     * return the value relative to the key pass
     * @param $key string key
     * @return mixed
     * @throws Exception
     */
    public static function get($key)
    {
        if(empty($key) || !is_string($key) || !isset($_POST[$key]))
            throw new Exception("the key '".$key."' is invalid !");

        return $_POST[$key];

    }

    /**
     * return all post value
     * @return mixed
     */
    public static function getAll()
    {
        return $_POST;
    }

    /**
     * set the value for the key pass
     * @param $key
     * @param $value
     * @return Exception
     */
    public static function set($key, $value)
    {
        if(empty($key) || !is_string($key))
            return new Exception("the key '".$key."' is invalid or empty !");

        if(empty($value))
            return new Exception("the value '".$value."' is empty !");

        $_POST[$key] = $value;
    }

    /**
     * check if the key exist
     * @param $key
     * @return bool
     */
    public static function exist($key)
    {
        if(!empty($key) && is_string($key) && isset($_POST[$key]))
            return true;
        else
            return false;
    }

    /**
     * return total count value of the $_POST
     * @return int
     */
    public static function count()
    {
        return count($_POST);
    }

}