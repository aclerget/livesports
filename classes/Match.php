<?php
/**
 * User: Maxwell
 * Date: 05/02/2015
 * Time: 19:25
 */
namespace classes;

use Db\Query as DB;


class Match {

    /**
     * @param $name
     * @param $teamA
     * @param $teamB
     * @param $timeToStart when the match started (string date)
     * @param $teamAScore
     * @param $teamBScore
     * @return mixed
     */
    public function addMatch($name, $teamA, $teamB, $timeToStart, $teamAScore, $teamBScore)
    {
        DB::insert('match',array(
                'name' => mysql_real_escape_string($name),
                'teamA' => mysql_real_escape_string($teamA),
                'teamB' => mysql_real_escape_string($teamB),
                'timeToStart' => mysql_real_escape_string($timeToStart),
                'teamAScore' => mysql_real_escape_string($teamAScore),
                'teamBScore' => mysql_real_escape_string($teamBScore),
            )
        );

        $id = DB::insert_id();

        //return the id if it's insert else false
        if(!empty($id))
            return $id;
        else
            return false;
    }

    /**
     * return the math of the id pass
     * @param $id
     * @return mixed
     */
    public function getMatch($id)
    {

        $result = DB::where('id', $id)->get('message');

        return $result->result_array();

    }

    /**
     * return list of matches
     * @return mixed
     */
    public function getMatches()
    {
        $result = DB::order_by('id','DESC')->get('message');

        return $result->result_array();

    }
}