<?php
/**
 * User: Maxwell
 * Date: 05/02/2015
 * Time: 19:25
 */
namespace classes;

use Db\Query as DB;


class Message {

    /**
     * @param $user
     * @param $message
     * @return mixed
     */
    public function addMessage($user, $message)
    {
        DB::insert('message',array(
                'username' => mysql_real_escape_string($user),
                'message_content' => mysql_real_escape_string($message)
            )
        );

        $id = DB::insert_id();
        //return the id if it's insert else false
        if(!empty($id))
            return $id;
        else
            return false;
    }

    /**
     * @param $id
     * @param $user
     * @param $message
     */
    public function updateMessage($id, $user, $message)
    {
        $id = mysql_real_escape_string($id);
        $user = mysql_real_escape_string($user);
        $message = mysql_real_escape_string($message);


        DB::where('id', $id)
            ->update('message', array(
                    'username' => $user,
                    'message' => $message
                )
            );

    }

    /**
     * @param int $limit
     */
    public function getMessages($limit = 15)
    {

        $result = DB::limit($limit)->order_by('id','DESC')->get('message');

        return $result->result_array();

    }
}