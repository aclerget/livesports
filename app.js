/* CONSTANT */
const PROJECT_FOLDER_NAME = '/' + 'livesports';
/* END CONSTANT */


var app = angular.module('MainApplication', ['ngRoute', 'ui.unique']);
app.config(function($routeProvider){
    $routeProvider.when('/', {templateUrl: 'view/home.html', controller: 'MainController'})
        .when('/add-message', {templateUrl: 'view/add_message.html', controller: 'AddMessageController'})
        .otherwise({redirectTo: '/'});
});

app.controller('MainController', function($scope, MessageManagerFactory, $interval)
{

    $interval(function () {
        MessageManagerFactory.getList(15).success(function (data) {
            console.log(data.messages);
            $scope.messages = data.messages;
        });
    }, 5000);

});

app.controller('AddMessageController', function($scope, MessageManagerFactory)
{
    $scope.messageInformation = {};

    //call when the user click to the save button
    $scope.submitForm = function(message)
    {
        $scope.messageInformation = angular.copy(message);
        MessageManagerFactory.addMessage($scope.messageInformation);
        $scope.resetForm();

    }

    //clear the form
    $scope.resetForm = function ()
    {
        $scope.message = {};
    }
});

//the manager of the Message
app.factory('MessageManagerFactory', function($http)
{
    return {
        addMessage: function(message){

            //add action
            message.action = 'add';

            //call ajax method for add the message
            $http.post(PROJECT_FOLDER_NAME+'/ajax/message/updateMessage.php', message).
                success(function(data, status, headers, config) {
                    if(data.error)
                    {

                    }
                    else if(data.success)
                    {
                        //data.success;
                    }

                }).
                error(function(data, status, headers, config) {
                    console.log('error into updateMessage.php !');
                });
        },
        updateMessage: function(id, message){
            //call ajax method for update the message
        },
        deleteMessage: function(id)
        {
            //remove the message
        },
        getList: function(limitOfMessages)
        {
           return $http.post(PROJECT_FOLDER_NAME+'/ajax/message/getMessages.php', {limit: limitOfMessages });
        }
    }
});