<?php

//include auto loader
$loader = require_once __DIR__.'/vendor/autoload.php';

CONST DEBUG = '0';

if(DEBUG == 1)
{
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(-1);
}
else{
    ini_set('display_errors',0);
    ini_set('display_startup_errors',0);
    error_reporting(0);
}