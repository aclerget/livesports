<?php
header('Content-Type: application/json');

require '../../app.php';

use classes\Tools;
use classes\Message;

$params = Tools::getDataToJsonRequest();

/* INIT */
$message = new Message();

if(!isset($params->action))
    json_encode(array('error' => 'no action'));

if(!isset($params->message))
    json_encode(array('error' => 'no message'));

switch($params->action)
{
    case 'add':
        $response = $message->addMessage($params->pseudo, $params->content);
        //check if we have an "id" or "false" response
        if($response != false)
            echo json_encode(array('success' => 'The message as successfully added !'));
        else
            echo json_encode(array('error' => 'The message has not insered !'));
        break;
    case 'update':
        $message->updateMessage($params->id, $params->pseudo, $params->content);
    break;
    case 'delete':

    break;
}