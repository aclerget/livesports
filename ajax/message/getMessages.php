<?php
header('Content-Type: application/json');

require '../../app.php';

use classes\Tools;
use classes\Message;

$params = Tools::getDataToJsonRequest();

/* INIT */
$message = new Message();
$allMessages = $message->getMessages($params->limit);

if(!empty($allMessages))
    echo json_encode(array('success' => 'the get is successfully done', 'messages' => $allMessages));
else
    echo json_encode(array('error' => 'no messages'));


exit;